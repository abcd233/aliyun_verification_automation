# 阿里云滑块验证自动化

#### 介绍
使用Selenium+Python+Undetected-Chromedriver
反反检测跳过阿里云系的滑块验证
默认应用于职教云验证

#### 软件架构
Selenium 4.2.0
python 3.9.8
Undetected-Chromedriver 3.1.0 
Github链接https://github.com/ultrafunkamsterdam/undetected-chromedriver


#### 安装教程

pycharm运行main.py或pyinstaller -F main.py 打包运行


#### 使用说明

1.请添加以下依赖：
altgraph==0.17.2
async-generator==1.10
attrs==21.4.0
certifi==2022.5.18.1
cffi==1.15.0
charset-normalizer==2.0.12
cryptography==37.0.2
docopt==0.6.2
future==0.18.2
h11==0.13.0
idna==3.3
MouseInfo==0.1.3
outcome==1.1.0
pefile==2022.5.30
Pillow==9.1.1
pipreqs==0.4.11
PyAutoGUI==0.9.53
pycparser==2.21
PyGetWindow==0.0.9
pyinstaller==5.1
pyinstaller-hooks-contrib==2022.7
PyMsgBox==1.0.9
pyOpenSSL==22.0.0
pyperclip==1.8.2
PyRect==0.2.0
PyScreeze==0.1.28
PySocks==1.7.1
pytweening==1.0.4
pywin32-ctypes==0.2.0
requests==2.27.1
selenium==4.2.0
sniffio==1.2.0
sortedcontainers==2.4.0
trio==0.21.0
trio-websocket==0.9.2
undetected-chromedriver==3.1.5.post4
urllib3==1.26.9
websockets==10.3
wsproto==1.1.0
yarg==0.1.9
