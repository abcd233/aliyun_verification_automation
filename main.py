# coding=gb2312
import json
import os
import sys
import traceback
import webbrowser
from tkinter import messagebox

import pyperclip
import undetected_chromedriver as uc
import tkinter as tk
import tkinter.font as tkFont
from telnetlib import EC
import ddddocr
from PIL import Image, ImageTk
from selenium.common import TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.devtools.v102 import browser
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
import threading
import time

event = threading.Event()
image = None
im = None
image1 = None
im1 = None


def fmtTime(timeStamp):
    timeArray = time.localtime(timeStamp)
    dateTime = time.strftime("%H:%M:%S", timeArray)
    return dateTime


class App:
    def __init__(self, root):
        self.initGUI(root)

    def initGUI(self, root):
        # setting title
        root.title("验证码滑块v1.4.0")
        # setting window size
        width = 350
        height = 504
        screenwidth = root.winfo_screenwidth()
        screenheight = root.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        root.geometry(alignstr)
        root.resizable(width=False, height=False)

        GLabel_666 = tk.Label(root)
        GLabel_666["anchor"] = "center"
        GLabel_666["bg"] = "#999999"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_666["font"] = ft
        GLabel_666["fg"] = "#333333"
        GLabel_666["justify"] = "center"
        GLabel_666["text"] = "仅供交流与使用，禁止用于非法用途"
        GLabel_666.place(x=0, y=0, width=350, height=25)

        GButton_726 = tk.Button(root)
        GButton_726["bg"] = "#5fb878"
        ft = tkFont.Font(family='Times', size=13)
        GButton_726["font"] = ft
        GButton_726["fg"] = "#ffffff"
        GButton_726["justify"] = "center"
        GButton_726["text"] = "教程&设置"
        GButton_726["relief"] = "flat"
        GButton_726.place(x=125, y=90, width=100, height=50)
        GButton_726["command"] = self.GButton_726_command

        GButton_963 = tk.Button(root)
        GButton_963["activebackground"] = "#ff5722"
        GButton_963["bg"] = "#ff5722"
        ft = tkFont.Font(family='Times', size=13)
        GButton_963["font"] = ft
        GButton_963["fg"] = "#ffffff"
        GButton_963["justify"] = "center"
        GButton_963["text"] = "更新&反馈"
        GButton_963["relief"] = "flat"
        GButton_963.place(x=125, y=210, width=100, height=50)
        GButton_963["command"] = self.GButton_963_command

        GLabel_15 = tk.Label(root)
        GLabel_15["bg"] = "#adadad"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_15["font"] = ft
        GLabel_15["fg"] = "#333333"
        GLabel_15["justify"] = "center"
        GLabel_15["text"] = "作者：ACT丶流星雨"
        GLabel_15.place(x=0, y=25, width=350, height=25)

        GLabel_641 = tk.Button(root)
        GLabel_641["bg"] = "#d6d6d6"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_641["font"] = ft
        GLabel_641["fg"] = "#333333"
        GLabel_641["justify"] = "center"
        GLabel_641["relief"] = "flat"
        GLabel_641["text"] = "主页：https://act_meteor.rth7.com/(点击跳转)"
        GLabel_641.place(x=0, y=50, width=350, height=25)
        GLabel_641["command"] = self.GLabel_15_command

        GLabel_37 = tk.Button(root)
        GLabel_37["bg"] = "#00babd"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_37["font"] = ft
        GLabel_37["fg"] = "#333333"
        GLabel_37["justify"] = "center"
        GLabel_37["relief"] = "flat"
        GLabel_37["text"] = "运行状态（点我复制）"
        GLabel_37.place(x=0, y=270, width=350, height=25)
        GLabel_37["command"] = self.Operating

        self.GButton_106 = tk.Button(root)
        self.GButton_106["activebackground"] = "#ffb800"
        self.GButton_106["activeforeground"] = "#1e9fff"
        self.GButton_106["bg"] = "#1e9fff"
        ft = tkFont.Font(family='Times', size=13)
        self.GButton_106["font"] = ft
        self.GButton_106["fg"] = "#ffffff"
        self.GButton_106["justify"] = "center"
        self.GButton_106["text"] = "启动程序"
        self.GButton_106["relief"] = "flat"
        self.GButton_106.place(x=125, y=150, width=100, height=50)
        self.GButton_106["command"] = self.start

        self.GMessage_822 = tk.Text(root)
        ft = tkFont.Font(family='Times', size=10)
        self.GMessage_822["font"] = ft
        self.GMessage_822["fg"] = "#333333"
        self.GMessage_822.place(x=0, y=295, width=351, height=205)
        self.GMessage_822.insert('end', '■■本插件仅用于Chrome浏览器■■' + '\n')
        self.GMessage_822.insert('end', '■■请务必查看使用教程■■' + '\n')
        BASE_DIR = os.path.dirname(os.path.realpath(sys.argv[0]))
        if os.access(f'{BASE_DIR}\\验证码滑块配置文件.json', os.F_OK):
            desktop_path = f'{BASE_DIR}\\'
            self.GMessage_822.insert('end', fmtTime(time.time()) + '已加载配置文件' + '\n')
            self.GMessage_822.insert('end', fmtTime(time.time()) + '加载目录：' + desktop_path + '验证码滑块配置文件.json\n')
            full_path = desktop_path + '验证码滑块配置文件' + '.json'
            with open(full_path, 'r', encoding='utf8') as f:
                a = json.load(f)
            if a['version'] == '1.4.0':
                pass
            else:
                content = {"username": a['username'], "password": a['password'],
                           "url": a['url'], "version": "1.4.0"}
                with open(full_path, 'w', encoding='utf8') as filename:
                    json.dump(content, filename)
                messagebox.showinfo("已更新V1.4.0", "本次更新完善了全部错误的可能，误操作闪退等都有相应提醒")
                filename.close()

        else:
            self.GMessage_822.insert('end', fmtTime(time.time()) + '正在创建配置文件' + '\n')
            desktop_path = f'{BASE_DIR}\\'
            self.GMessage_822.insert('end', fmtTime(time.time()) + '创建目录：' + desktop_path + '验证码滑块配置文件.json\n')
            full_path = desktop_path + '验证码滑块配置文件' + '.json'
            content = {"username": "未配置", "password": "未配置", "url": "未配置", "version": "1.4.0"}
            with open(full_path, 'w', encoding='utf8') as filename:
                json.dump(content, filename)
            filename.close()
            self.GMessage_822.insert('end', fmtTime(time.time()) + '创建完成' + '\n')
            messagebox.showinfo("版本更新V1.4.0", "本次更新完善了全部错误的可能，误操作闪退等都有相应提醒")
        root.mainloop()
        if os.access(f'{BASE_DIR}\\验证码图片.png', os.F_OK):
            os.remove("验证码图片.png")

    def Operating(self):
        text = self.GMessage_822.get('0.0', 'end')
        pyperclip.copy(text)
        messagebox.showinfo("提示", "复制成功")

    def GButton_726_command(self):
        self.top = tk.Toplevel()
        self.top.title('如何使用？')
        width = 350
        height = 330
        screenwidth = self.top.winfo_screenwidth()
        screenheight = self.top.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.top.geometry(alignstr)
        self.top.resizable(width=False, height=False)

        GLabel_4780 = tk.Label(self.top)
        GLabel_4780["bg"] = "#009688"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_4780["font"] = ft
        GLabel_4780["fg"] = "#ffffff"
        GLabel_4780["justify"] = "center"
        GLabel_4780["text"] = "1、请使用Chrome浏览器，不可以使用其他浏览器"
        GLabel_4780.place(x=0, y=0, width=350, height=30)

        GButton_2830 = tk.Button(self.top)
        GButton_2830["bg"] = "#01aaed"
        ft = tkFont.Font(family='Times', size=10)
        GButton_2830["font"] = ft
        GButton_2830["fg"] = "#000000"
        GButton_2830["justify"] = "center"
        GButton_2830["text"] = "点我下载浏览器"
        GButton_2830["relief"] = "flat"
        GButton_2830.place(x=130, y=40, width=100, height=25)
        GButton_2830["command"] = self.GButton_283_command

        GLabel_9780 = tk.Label(self.top)
        GLabel_9780["bg"] = "#009688"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_9780["font"] = ft
        GLabel_9780["fg"] = "#ffffff"
        GLabel_9780["justify"] = "center"
        GLabel_9780["text"] = "2、点击启动程序"
        GLabel_9780.place(x=0, y=70, width=350, height=30)

        GLabel_9780 = tk.Label(self.top)
        GLabel_9780["bg"] = "#009688"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_9780["font"] = ft
        GLabel_9780["fg"] = "#ffffff"
        GLabel_9780["justify"] = "center"
        GLabel_9780["text"] = "3、等待职教云网页出现，期间请勿点击鼠标，切勿关闭窗口"
        GLabel_9780.place(x=0, y=100, width=350, height=30)

        GLabel_6520 = tk.Label(self.top)
        GLabel_6520["bg"] = "#009688"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_6520["font"] = ft
        GLabel_6520["fg"] = "#ffffff"
        GLabel_6520["justify"] = "center"
        GLabel_6520["text"] = "4、使用期间务必全屏且前台运行"
        GLabel_6520.place(x=0, y=130, width=350, height=30)

        GLabel_2320 = tk.Label(self.top)
        GLabel_2320["bg"] = "#009688"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_2320["font"] = ft
        GLabel_2320["fg"] = "#ffffff"
        GLabel_2320["justify"] = "center"
        GLabel_2320["text"] = "5、如果遇到问题，请回退到上个版本"
        GLabel_2320.place(x=0, y=160, width=350, height=30)

        GLabel_1010 = tk.Label(self.top)
        GLabel_1010["bg"] = "#009688"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_1010["font"] = ft
        GLabel_1010["fg"] = "#ffffff"
        GLabel_1010["justify"] = "center"
        GLabel_1010["text"] = "5、无法拉起请确认下载了上面的浏览器，速度与配置相关"
        GLabel_1010.place(x=0, y=190, width=350, height=30)

        GLabel_1010 = tk.Label(self.top)
        GLabel_1010["bg"] = "#009688"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_1010["font"] = ft
        GLabel_1010["fg"] = "#ffffff"
        GLabel_1010["justify"] = "center"
        GLabel_1010["text"] = "如果还不行可以加我QQ：1340145680"
        GLabel_1010.place(x=0, y=220, width=350, height=30)

        GButton_2830 = tk.Button(self.top)
        GButton_2830["bg"] = "#ff5722"
        ft = tkFont.Font(family='Times', size=10)
        GButton_2830["font"] = ft
        GButton_2830["fg"] = "#000000"
        GButton_2830["justify"] = "center"
        GButton_2830["text"] = "设置"
        GButton_2830["relief"] = "flat"
        GButton_2830.place(x=130, y=260, width=100, height=25)
        GButton_2830["command"] = self.GButton_2830_command

        GButton_283 = tk.Button(self.top)
        GButton_283["bg"] = "#01aaed"
        ft = tkFont.Font(family='Times', size=10)
        GButton_283["font"] = ft
        GButton_283["fg"] = "#000000"
        GButton_283["justify"] = "center"
        GButton_283["text"] = "联系作者"
        GButton_283["relief"] = "flat"
        GButton_283.place(x=130, y=300, width=100, height=25)
        GButton_283["command"] = self.GButton_28_command

    def GButton_2830_command(self):
        self.top.destroy()
        self.top3 = tk.Toplevel()
        # setting title
        self.top3.title("自动填充配置")
        # setting window size
        width = 350
        height = 270
        screenwidth = self.top3.winfo_screenwidth()
        screenheight = self.top3.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.top3.geometry(alignstr)
        self.top3.resizable(width=False, height=False)

        self.GMessage_353 = tk.Entry(self.top3)
        self.GMessage_353["bg"] = "#ffffff"
        ft = tkFont.Font(family='Times', size=10)
        self.GMessage_353["font"] = ft
        self.GMessage_353["fg"] = "#333333"
        self.GMessage_353.place(x=70, y=40, width=250, height=30)

        self.GMessage_829 = tk.Entry(self.top3)
        self.GMessage_829["bg"] = "#ffffff"
        ft = tkFont.Font(family='Times', size=10)
        self.GMessage_829["font"] = ft
        self.GMessage_829["fg"] = "#333333"
        self.GMessage_829.place(x=70, y=100, width=250, height=30)

        GButton_384 = tk.Button(self.top3)
        GButton_384["bg"] = "#ffb800"
        ft = tkFont.Font(family='Times', size=10)
        GButton_384["font"] = ft
        GButton_384["fg"] = "#000000"
        GButton_384["justify"] = "center"
        GButton_384["text"] = "保存"
        GButton_384["relief"] = "flat"
        GButton_384.place(x=140, y=220, width=70, height=25)
        GButton_384["command"] = self.GButton_384_command

        self.GMessage_428 = tk.Entry(self.top3)
        self.GMessage_428["bg"] = "#ffffff"
        ft = tkFont.Font(family='Times', size=10)
        self.GMessage_428["font"] = ft
        self.GMessage_428["fg"] = "#333333"
        self.GMessage_428.place(x=70, y=160, width=250, height=30)

        GLabel_605 = tk.Label(self.top3)
        ft = tkFont.Font(family='Times', size=10)
        GLabel_605["font"] = ft
        GLabel_605["fg"] = "#333333"
        GLabel_605["justify"] = "center"
        GLabel_605["text"] = "账号："
        GLabel_605.place(x=0, y=40, width=70, height=25)

        GLabel_196 = tk.Label(self.top3)
        ft = tkFont.Font(family='Times', size=10)
        GLabel_196["font"] = ft
        GLabel_196["fg"] = "#333333"
        GLabel_196["justify"] = "center"
        GLabel_196["text"] = "密码："
        GLabel_196.place(x=0, y=100, width=70, height=25)

        GLabel_423 = tk.Label(self.top3)
        ft = tkFont.Font(family='Times', size=10)
        GLabel_423["font"] = ft
        GLabel_423["fg"] = "#333333"
        GLabel_423["justify"] = "center"
        GLabel_423["text"] = "脚本地址："
        GLabel_423.place(x=0, y=160, width=70, height=25)

        GLabel_11 = tk.Label(self.top3)
        GLabel_11["bg"] = "#5fb878"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_11["font"] = ft
        GLabel_11["fg"] = "#ffffff"
        GLabel_11["justify"] = "center"
        GLabel_11["text"] = "本页设置自动填充，地址不指定则默认打开推荐的刷课脚本"
        GLabel_11.place(x=0, y=0, width=350, height=30)

        BASE_DIR = os.path.dirname(os.path.realpath(sys.argv[0]))
        if os.access(f'{BASE_DIR}/验证码滑块配置文件.json', os.F_OK):
            desktop_path = f'{BASE_DIR}/'
            full_path = desktop_path + '验证码滑块配置文件' + '.json'
            with open(full_path, 'r', encoding='utf8') as f:
                a = json.load(f)
            self.GMessage_353.insert('0', a['username'])
            self.GMessage_829.insert('0', a['password'])
            self.GMessage_428.insert('0', a['url'])

    def GButton_384_command(self):
        BASE_DIR = os.path.dirname(os.path.realpath(sys.argv[0]))
        desktop_path = f'{BASE_DIR}/'
        full_path = desktop_path + '验证码滑块配置文件' + '.json'
        if self.GMessage_353.get() == '':
            a = '未配置'
        else:
            a = self.GMessage_353.get()
        if self.GMessage_829.get() == '':
            b = '未配置'
        else:
            b = self.GMessage_829.get()
        if self.GMessage_428.get() == '':
            c = '未配置'
        else:
            c = self.GMessage_428.get()
        content = {"username": a, "password": b,
                   "url": c, "version": "1.4.0"}
        with open(full_path, 'w', encoding='utf8') as filename:
            json.dump(content, filename)
        filename.close()
        self.top.destroy()
        self.top3.destroy()
        messagebox.showinfo("提示", "保存成功")

    def GButton_283_command(self):
        webbrowser.open("https://www.google.cn/intl/zh-CN/chrome/")

    def GButton_28_command(self):
        webbrowser.open('http://wpa.qq.com/msgrd?v=3&uin=1340145680&site=qq&menu=yes')

    def GLabel_15_command(self):
        webbrowser.open('https://act_meteor.rth7.com/')

    def GButton_963_command(self):
        self.top2 = tk.Toplevel()
        self.top2.title("更新&反馈")
        width = 300
        height = 270
        screenwidth = self.top2.winfo_screenwidth()
        screenheight = self.top2.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.top2.geometry(alignstr)
        self.top2.resizable(width=False, height=False)

        GLabel_668 = tk.Label(self.top2)
        GLabel_668["bg"] = "#999999"
        ft = tkFont.Font(family='Times', size=10)
        GLabel_668["font"] = ft
        GLabel_668["fg"] = "#ffffff"
        GLabel_668["justify"] = "center"
        GLabel_668["text"] = "更新&反馈"
        GLabel_668.place(x=0, y=0, width=300, height=30)

        GButton_741 = tk.Button(self.top2)
        GButton_741["bg"] = "#1e9fff"
        ft = tkFont.Font(family='Times', size=10)
        GButton_741["font"] = ft
        GButton_741["fg"] = "#000000"
        GButton_741["justify"] = "center"
        GButton_741["text"] = "更新"
        GButton_741["relief"] = "flat"
        GButton_741.place(x=70, y=40, width=70, height=25)
        GButton_741["command"] = self.GButton_741_command

        GButton_759 = tk.Button(self.top2)
        GButton_759["bg"] = "#5fb878"
        ft = tkFont.Font(family='Times', size=10)
        GButton_759["font"] = ft
        GButton_759["fg"] = "#000000"
        GButton_759["justify"] = "center"
        GButton_759["text"] = "反馈"
        GButton_759["relief"] = "flat"
        GButton_759.place(x=160, y=40, width=70, height=25)
        GButton_759["command"] = self.GButton_759_command

        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        global image
        global im
        global image1
        global im1
        GLabel_60 = tk.Canvas(self.top2, bg='green', height=140, width=140)
        image = Image.open(f"{BASE_DIR}/img/wx.png")
        im = ImageTk.PhotoImage(image)
        GLabel_60.create_image(70, 70, image=im)
        GLabel_60.place(x=5, y=120, width=140, height=140)

        GLabel_388 = tk.Canvas(self.top2, bg='green', height=140, width=140)
        image1 = Image.open(f"{BASE_DIR}/img/zfb.png")
        im1 = ImageTk.PhotoImage(image1)
        GLabel_388.create_image(70, 70, image=im1)
        GLabel_388.place(x=155, y=120, width=140, height=140)

        GLabel_159 = tk.Label(self.top2)
        GLabel_159["bg"] = "#ff5722"
        ft = tkFont.Font(family='Times', size=13)
        GLabel_159["font"] = ft
        GLabel_159["fg"] = "#ffffff"
        GLabel_159["justify"] = "center"
        GLabel_159["text"] = "打赏作者"
        GLabel_159.place(x=0, y=80, width=300, height=30)

    def GButton_741_command(self):
        webbrowser.open('https://act_meteor.rth7.com/cj.html')
        self.top2.destroy()

    def GButton_759_command(self):
        webbrowser.open('https://shimo.im/forms/9PDYlsebSAUa4ma8/fill')
        self.top2.destroy()

    def GButton_106_command(self):
        try:
            result = tk.messagebox.showinfo(title='提醒',
                                            message='注意：请勿操作浏览器和点击鼠标\n本版本为全自动操作，请耐心等待直到职教云网站出来\n务必全屏浏览器并且放置前台')
            if result:
                T = threading.Thread(target=self.timeset, args=())
                T.start()
                T = threading.Thread(target=self.find_main, args=())
                T.start()
            else:
                return
        except Exception as e:
            self.GMessage_822.insert('end', fmtTime(time.time()) + '多线程异常：' + str(e) + '\n')
            return

    def timeset(self):
        self.GMessage_822.delete('1.0', 'end')
        try:
            os.system('taskkill /f /t /im chrome.exe')
            self.GMessage_822.insert('end', fmtTime(time.time()) + '正在关闭Chrome进程...' + '\n')
        except:
            pass
        self.GMessage_822.insert('end', fmtTime(time.time()) + '正在准备....' + '\n')
        self.GButton_106['state'] = 'disabled'
        self.GMessage_822.insert('end', fmtTime(time.time()) + '开始执行启动程序：' + '\n\n')
        self.GMessage_822.insert('end', fmtTime(
            time.time()) + '正在拉起Chrome浏览器....\n■拉起时间由机器配置决定■\n※浏览器闪退、无法拉起，请到教程重新下载安装浏览器※' + '\n')

    def start(self):
        T = threading.Thread(target=self.GButton_106_command, args=())
        T.start()

    def find_main(self):
        global kill, Tampermonkey, key
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        chrome_options = uc.ChromeOptions()
        chrome_options.add_argument(f'--load-extension={BASE_DIR}/pack')
        # 实例化
        try:
            driver = uc.Chrome(use_subprocess=True, options=chrome_options)
        except:
            self.GMessage_822.insert('end', fmtTime(time.time()) + '错误，无法配置Chrome浏览器' + '\n')
            messagebox.showinfo("运行异常！", "无法找到Chrome浏览器，请点击教程前往下载。")
            return
        driver.set_page_load_timeout(15)
        # 全屏窗口
        driver.maximize_window()
        # 静态等待
        driver.implicitly_wait(1)
        # 读取配置文件
        BASE_DIRS = os.path.dirname(os.path.realpath(sys.argv[0]))
        desktop_path = f'{BASE_DIRS}/'
        full_path = desktop_path + '验证码滑块配置文件' + '.json'
        with open(full_path, 'r', encoding='utf8') as f:
            a = json.load(f)
        try:
            try:
                self.GMessage_822.insert('end', fmtTime(time.time()) + '打开成功...' + '\n')
                self.GMessage_822.insert('end', fmtTime(time.time()) + '开始执行自动模式' + '\n')
                self.GMessage_822.insert('end', fmtTime(time.time()) + '自动模式执行效率与网速相关' + '\n')
                if a['url'] != '未配置':
                    try:
                        driver.get(a['url'])
                    except TimeoutException:
                        self.GMessage_822.insert('end', fmtTime(time.time()) + '异常：浏览器超时' + '\n')
                        messagebox.showinfo("超时！", "处理时间超过15秒！")
                        result = messagebox.askokcancel("问题处理",
                                                        "问题一：浏览器已拉起，但页面没反应\n          导致原因：脚本网页还在转圈圈加载中，得等加载好才能继续\n     解决办法：更换网络重试，或者重新启动。\n问题二：浏览器无法拉起\n          导致原因：浏览器版本无法找到对应驱动\n     解决办法：请点击教程重新下载安装浏览器然后重试，还是无法拉起请卸载后再安装。如果问题仍然存在请点击更新-联系作者，寻求作者处理" + '\n' + '\n■■点击确认继续等待，取消则返回■■')
                        if result:
                            self.GMessage_822.insert('end', fmtTime(time.time()) + '已选择继续等待' + '\n')
                            driver.get(a['url'])
                        else:
                            self.GButton_106['state'] = 'normal'
                            self.GMessage_822.insert('end', fmtTime(time.time()) + '已选择取消等待' + '\n')
                            os.system('taskkill /f /t /im chrome.exe')
                            return
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '已打开脚本网站：' + a['url'] + '\n')
                else:
                    try:
                        driver.get('https://greasyfork.org/zh-CN/scripts/432222')
                    except TimeoutException:
                        self.GMessage_822.insert('end', fmtTime(time.time()) + '异常：浏览器超时' + '\n')
                        messagebox.showinfo("超时！", "处理时间超过15秒！")
                        result = messagebox.askokcancel("问题处理",
                                                        "问题一：浏览器已拉起，但页面没反应\n          导致原因：脚本网页还在转圈圈加载中，得等加载好才能继续\n     解决办法：更换网络重试，或者重新启动。\n问题二：浏览器无法拉起\n          导致原因：浏览器版本无法找到对应驱动\n     解决办法：请点击教程重新下载安装浏览器然后重试，还是无法拉起请卸载后再安装。如果问题仍然存在请点击更新-联系作者，寻求作者处理" + '\n' + '\n■■点击确认继续等待，取消则返回■■')
                        if result:
                            self.GMessage_822.insert('end', fmtTime(time.time()) + '已选择继续等待' + '\n')
                            driver.get('https://greasyfork.org/zh-CN/scripts/432222')
                        else:
                            self.GButton_106['state'] = 'normal'
                            self.GMessage_822.insert('end', fmtTime(time.time()) + '已选择取消等待' + '\n')
                            os.system('taskkill /f /t /im chrome.exe')
                            return
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '已打开默认脚本网站' + '\n')
                # 全自动装插件
                kill = driver.current_window_handle
                kill_title = driver.title
                self.GMessage_822.insert('end', fmtTime(time.time()) + '脚本名称：' + kill_title + '\n')
                self.GMessage_822.insert('end', fmtTime(time.time()) + '脚本页面句柄：' + kill + '\n')
                while True:
                    if len(driver.window_handles) >= 2:
                        if driver.window_handles[0] == kill:
                            Tampermonkey = driver.window_handles[1]
                        else:
                            Tampermonkey = driver.window_handles[0]
                        driver.switch_to.window(Tampermonkey)
                        self.GMessage_822.insert('end', fmtTime(time.time()) + '油猴官网页面句柄：' + Tampermonkey + '\n')
                        driver.close()
                        self.GMessage_822.insert('end', fmtTime(time.time()) + '已打关闭油猴官网' + '\n')
                        driver.switch_to.window(kill)
                        WebDriverWait(driver, 9999).until(
                            EC.element_to_be_clickable((By.XPATH, '//*[@id="install-area"]/a[1]'))).click()
                        while True:
                            if len(driver.window_handles) >= 2:
                                if driver.window_handles[0] == kill:
                                    key = driver.window_handles[1]
                                else:
                                    key = driver.window_handles[0]
                                self.GMessage_822.insert('end', fmtTime(time.time()) + '安装页面句柄：' + key + '\n')
                                break
                    break
                driver.switch_to.window(key)
                WebDriverWait(driver, 9999).until(
                    EC.element_to_be_clickable((By.XPATH, '//*[@id="input_icVfdW5kZWZpbmVk_bu"]'))).click()
                self.GMessage_822.insert('end', fmtTime(time.time()) + '已打安装脚本' + '\n')
                # 打开网页
                driver.switch_to.window(driver.window_handles[0])
                self.GMessage_822.insert('end', fmtTime(time.time()) + '使用脚本：' + kill_title + '\n')
                try:
                    driver.get('https://zjy2.icve.com.cn/student/studio/studio.html')
                except TimeoutException:
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '异常：浏览器超时' + '\n')
                    messagebox.showinfo("超时！", "处理时间超过15秒！")
                    result = messagebox.askokcancel("问题处理",
                                                    "问题一：浏览器已拉起，但页面没反应\n          导致原因：脚本网页还在转圈圈加载中，得等加载好才能继续\n     解决办法：更换网络重试，或者重新启动。\n问题二：浏览器无法拉起\n          导致原因：浏览器版本无法找到对应驱动\n     解决办法：请点击教程重新下载安装浏览器然后重试，还是无法拉起请卸载后再安装。如果问题仍然存在请点击更新-联系作者，寻求作者处理" + '\n' + '\n■■点击确认继续等待，取消则返回■■')
                    if result:
                        self.GMessage_822.insert('end', fmtTime(time.time()) + '已选择继续等待' + '\n')
                        driver.get('https://zjy2.icve.com.cn/student/studio/studio.html')
                    else:
                        self.GButton_106['state'] = 'normal'
                        self.GMessage_822.insert('end', fmtTime(time.time()) + '已选择取消等待' + '\n')
                        os.system('taskkill /f /t /im chrome.exe')
                        return
                self.GMessage_822.insert('end', fmtTime(time.time()) + '已打打开职教云' + '\n')
                self.GButton_106['state'] = 'normal'
                self.GMessage_822.insert('end', fmtTime(time.time()) + '正在准备登录...' + '\n')
                if a['username'] != '未配置':
                    driver.find_element(By.XPATH, '//*[@id="login-tabs"]/div/div[1]/div/div[1]/div/input').send_keys(
                        a['username'])
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '已自动输入账号' + '\n')
                else:
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '未配置账号' + '\n')
                if a['password'] != '未配置':
                    driver.find_element(By.XPATH, '//*[@id="x-modify"]/div/input[1]').send_keys(a['password'])
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '已自动输入密码' + '\n')
                else:
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '未配置密码' + '\n')
                # 自动识别验证码
                imgCode = driver.find_element(By.XPATH, '//*[@id="x-modify"]/div/img[2]')
                imgCode.screenshot("验证码图片.png")
                ocr = ddddocr.DdddOcr()
                full_img = desktop_path + '验证码图片' + '.png'
                with open(full_img, "rb") as fp:
                    image = fp.read()
                result = ocr.classification(image)
                driver.find_element(By.XPATH, '//*[@id="x-modify"]/div/input[2]').send_keys(result)
                self.GMessage_822.insert('end', fmtTime(time.time()) + '已自动输入验证码，请检查是否正确' + '\n')
                time.sleep(1)
                if a['password'] != '未配置' and a['username'] != '未配置':
                    WebDriverWait(driver, 9999).until(
                        EC.element_to_be_clickable((By.XPATH, '//*[@id="btnLogin"]'))).click()
            except Exception as e:
                try:
                    os.system('taskkill /f /t /im chrome.exe')
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '自动运行失败' + '\n')
                except:
                    pass
                print(e)
                e = str(e)
                if e.find('Session info: chrome=') != -1:
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '浏览器参数无法配置，已闪退。' + '\n')
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '错误代码：\n' + e + '\n')
                    result = messagebox.askokcancel('警告', '浏览器参数配置异常，导致闪退，请点击确定重新下载安装浏览器解决问题，如果是手动关闭浏览器请忽略本条警告。')
                    self.GButton_106['state'] = 'normal'
                    if result:
                        webbrowser.open("https://www.google.cn/intl/zh-CN/chrome/")
                    return
                else:
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '因为用户操作浏览器已弹出切换模式选择器。' + '\n')
                    result = messagebox.askokcancel("请仔细阅读教程！",
                                                    "你因为操作鼠标或点击浏览器，自动运行失败" + '\n' + '点击确定切换为手动模式，手动模式请自行点击安装脚本' + '\n' + '点击取消然后再次运行则重新运行自动模式，自动模式请耐心等待直到职教云网页出现，期间请勿操作浏览器')
                    if result:
                        try:
                            self.GMessage_822.insert('end', fmtTime(
                                time.time()) + '已点击确定，开始执行手动模式' + '\n' + '■请手动点击安装油猴脚本■' + '\n')
                            self.GMessage_822.insert('end', fmtTime(time.time()) + '正在拉起浏览器请稍后....' + '\n')
                            BASE_DIR = os.path.dirname(os.path.abspath(__file__))
                            chrome_options = uc.ChromeOptions()
                            chrome_options.add_argument(f'--load-extension={BASE_DIR}/pack')
                            driver = uc.Chrome(use_subprocess=True, options=chrome_options)
                            driver.maximize_window()
                            driver.implicitly_wait(1)
                            try:
                                driver.get('https://zjy2.icve.com.cn/student/studio/studio.html')
                            except TimeoutException:
                                self.GMessage_822.insert('end', fmtTime(time.time()) + '异常：浏览器超时' + '\n')
                                messagebox.showinfo("超时！", "处理时间超过15秒！")
                                result = messagebox.askokcancel("问题处理",
                                                                "问题一：浏览器已拉起，但页面没反应\n     导致原因：脚本网页还在转圈圈加载中，得等加载好才能继续\n     解决办法：更换网络重试，或者重新启动。\n问题二：浏览器无法拉起\n     导致原因：浏览器版本无法找到对应驱动\n     解决办法：请点击教程重新下载安装浏览器然后重试，还是无法拉起请卸载后再安装。如果问题仍然存在请点击更新-联系作者，寻求作者处理" + '\n' + '\n■■点击确认继续等待，取消则返回■■')
                                if result:
                                    self.GMessage_822.insert('end', fmtTime(time.time()) + '已选择继续等待' + '\n')
                                    driver.get('https://zjy2.icve.com.cn/student/studio/studio.html')
                                else:
                                    self.GButton_106['state'] = 'normal'
                                    self.GMessage_822.insert('end', fmtTime(time.time()) + '已选择取消等待' + '\n')
                                    os.system('taskkill /f /t /im chrome.exe')
                                    return
                            self.GMessage_822.insert('end', fmtTime(time.time()) + '打开成功。' + '\n')
                            if a['url'] != '未配置':
                                driver.tab_new(a['url'])
                                self.GMessage_822.insert('end', fmtTime(time.time()) + '已打开脚本网站：' + a['url'] + '\n')
                            else:
                                driver.tab_new('https://greasyfork.org/zh-CN/scripts/432222')
                            self.GButton_106['state'] = 'normal'
                            self.GMessage_822.insert('end', fmtTime(time.time()) + '正在准备登录...' + '\n')
                            if a['username'] != '未配置':
                                driver.find_element(By.XPATH,
                                                    '//*[@id="login-tabs"]/div/div[1]/div/div[1]/div/input').send_keys(
                                    a['username'])
                                self.GMessage_822.insert('end', fmtTime(time.time()) + '已自动输入账号' + '\n')
                            else:
                                self.GMessage_822.insert('end', fmtTime(time.time()) + '未配置账号' + '\n')
                            if a['password'] != '未配置':
                                driver.find_element(By.XPATH, '//*[@id="x-modify"]/div/input[1]').send_keys(
                                    a['password'])
                                self.GMessage_822.insert('end', fmtTime(time.time()) + '已自动输入密码' + '\n')
                            else:
                                self.GMessage_822.insert('end', fmtTime(time.time()) + '未配置密码' + '\n')
                                # 自动识别验证码
                            imgCode = driver.find_element(By.XPATH, '//*[@id="x-modify"]/div/img[2]')
                            imgCode.screenshot("验证码图片.png")
                            ocr = ddddocr.DdddOcr()
                            full_img = desktop_path + '验证码图片' + '.png'
                            with open(full_img, "rb") as fp:
                                image = fp.read()
                            result = ocr.classification(image)
                            driver.find_element(By.XPATH, '//*[@id="x-modify"]/div/input[2]').send_keys(result)
                            self.GMessage_822.insert('end', fmtTime(time.time()) + '已自动输入验证码，请检查是否正确' + '\n')
                            time.sleep(1)
                            if a['password'] != '未配置' and a['username'] != '未配置':
                                WebDriverWait(driver, 9999).until(
                                    EC.element_to_be_clickable((By.XPATH, '//*[@id="btnLogin"]'))).click()
                        except Exception as e:
                            messagebox.showinfo("手动运行异常！", "错误代码已经打印在运行状态中。")
                            self.GMessage_822.insert('end', fmtTime(time.time()) + '手动模式无法打开浏览器，等待重试' + '\n')
                            self.GMessage_822.insert('end', fmtTime(time.time()) + '手动异常问题：' + str(e) + '\n')
                            self.GButton_106['state'] = 'normal'
                            return
                    else:
                        self.GMessage_822.insert('end', fmtTime(time.time()) + '已点击取消，等待用户点击启动程序' + '\n')
                        self.GButton_106['state'] = 'normal'
                        return
        except Exception as e:
            messagebox.showinfo("自动运行异常！", "错误代码已经打印在运行状态中，建议重新尝试。")
            self.GMessage_822.insert('end', fmtTime(time.time()) + '自动运行无法打开浏览器，等待重试' + '\n')
            self.GMessage_822.insert('end', fmtTime(time.time()) + '自动异常问题：' + str(e) + '\n')
            self.GButton_106['state'] = 'normal'
            return
        self.GMessage_822.insert('end', fmtTime(time.time()) + '开始监听验证滑块....' + '\n')
        # 执行“等待元素出现”方法
        wait = WebDriverWait(driver, 99999)

        # 处理函数
        def find_dom():
            # 判断元素是否存在
            element = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="nocaptcha"]')))
            if element:
                try:
                    demo0 = driver.find_element(By.XPATH, '//*[@id="nc_1_refresh1"]')
                    a = 0
                    while demo0:
                        time.sleep(1)
                        a += 1
                        WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.LINK_TEXT, "刷新"))).click()
                        self.GMessage_822.insert('end', fmtTime(time.time()) + '点击刷新，正在重试(' + a + ')\n')
                        if a > 20:
                            break
                except:
                    self.GMessage_822.insert('end', fmtTime(time.time()) + '开始滑动...' + '\n')
                    element0 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="nc_1__scale_text"]/span')))
                    if element0:
                        # 定位元素
                        text = driver.find_element(By.XPATH, '//*[@id="nc_1__scale_text"]/span')
                        demo = driver.find_element(By.XPATH, '//*[@id="nc_1_n1z"]')
                        # 如果元素存在则点击
                        if text.text == '请按住滑块，拖动到最右边' and demo:
                            action = ActionChains(driver)
                            action.click_and_hold(demo)
                            action.move_by_offset(400, 0)
                            action.release()
                            action.perform()
                            try:
                                demo0 = driver.find_element(By.XPATH, '//*[@id="nc_1_refresh1"]')
                                a = 0
                                while demo0:
                                    time.sleep(1)
                                    a += 1
                                    WebDriverWait(driver, 20).until(
                                        EC.element_to_be_clickable((By.LINK_TEXT, "刷新"))).click()
                                    self.GMessage_822.insert('end', fmtTime(time.time()) + '点击刷新，正在重试(' + a + ')\n')
                                    if a > 20:
                                        self.GMessage_822.insert('end', fmtTime(time.time()) + '重试超过20次' + '\n')
                                        messagebox.showinfo("警告！", "请手动点击刷新！")
                                        break
                            except:
                                self.GMessage_822.insert('end', fmtTime(time.time()) + '滑动成功，进行下一次监听' + '\n')
                                time.sleep(5)
                                find_dom()
                        else:
                            self.GMessage_822.insert('end', fmtTime(time.time()) + '元素不正确，已跳过' + '\n')
                            time.sleep(5)
                            find_dom()

        find_dom()


if __name__ == "__main__":
    root = tk.Tk()
    app = App(root)
